package org.com.softwareengineeringproject;

import org.junit.Test;

public class ServerTest {

	@Test
	public void testValidConstructor() {
		@SuppressWarnings("unused")
		Server server = new Server(1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidConstructor() {
		@SuppressWarnings("unused")
		Server server = new Server(-100);
	}
}
