package org.com.softwareengineeringproject;

import static org.junit.Assert.assertEquals;

import java.security.KeyPair;

import org.junit.Test;

public class RSATest {

	@Test
	public void testKeyPairGeneration() {
		KeyPair keyPair = RSA.getInstance().generateKeyPair();

		// Ensuring that a Public and a Private Key were generated using the RSA
		// Algorithm
		assertEquals("RSA", keyPair.getPublic().getAlgorithm());
		assertEquals("RSA", keyPair.getPrivate().getAlgorithm());
	}

	/**
	 * Black Box Testing
	 */
	@Test
	public void testRSAAlgorithm() {
		KeyPair keyPair = RSA.getInstance().generateKeyPair();

		String sampleText = "Apple";

		// Encoding sampleText String into a byte array
		byte[] encodedSampleTextBytes = RSA.getInstance().encrypt(keyPair.getPrivate(), sampleText);

		// Decoding sampleText byte array
		byte[] decodedSampleTextBytes = RSA.getInstance().decrypt(keyPair.getPublic(), encodedSampleTextBytes);

		assertEquals("Apple", new String(decodedSampleTextBytes));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidEncryption() {
		RSA.getInstance().encrypt(null, null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidDecryption() {
		RSA.getInstance().decrypt(null, null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidPublicKeyToString() {
		RSA.getInstance().publicKeyToString(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidStringToPublicKey() {
		RSA.getInstance().stringToPublicKey(null);
	}
	
}
