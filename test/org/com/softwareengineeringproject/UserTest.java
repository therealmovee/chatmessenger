package org.com.softwareengineeringproject;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UserTest {

	/**
	 * Testing the User Constructor
	 */
	
	@Test
	public void testUserConstructor() {
		User user = new User("Alexis", 0);
		assertEquals("Alexis", user.getNickname());
	}
	
	/**
	 * Testing an invalid user construction
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testInvalidUserConstructor() {
		@SuppressWarnings("unused")
		User user = new User(null, 0);
	}
}
	