package org.com.softwareengineeringproject;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AccessKeyTest {
	
	@Test
	public void testAccessKeyEncryption() {
		int samplePort = 5050;
		String encodedKey = AccessKey.getInstance().encodeAccessKey(samplePort);
		
		// Testing key encryption
		assertEquals("AA330956800", encodedKey);
		
		// Testing key decryption
		assertEquals(5050, AccessKey.getInstance().decodeAccessKey(encodedKey));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidAccessKey() {
		AccessKey.getInstance().decodeAccessKey(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidPort() {
		AccessKey.getInstance().encodeAccessKey(-123);
	}
}
