/**
 * User.java
 */
package org.com.softwareengineeringproject;

/**
 * @author ac01271
 */

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.PrivateKey;
import java.security.PublicKey;

public class User implements SocketClass {

	/**
	 * Variable used to store the Data Input Stream of the User
	 */
	private DataInputStream dataIn = null;

	/**
	 * Variable used to store the Data Output Stream of the User
	 */
	private DataOutputStream dataOut = null;

	/**
	 * Variable used to store the Public Key of the User
	 */
	private PublicKey publicKey = null;

	/**
	 * Variable used to store the Private Key of the User
	 */
	private PrivateKey privateKey = null;

	/**
	 * Variable used to store the Public Key of the Server
	 */
	private PublicKey guestPublicKey = null;

	/**
	 * Socket used to control the incoming and out-coming traffic of the User
	 */
	private Socket socket = null;

	/**
	 * The nickname of the User
	 */
	private String userNickname = null;

	/**
	 * Variable used to store the port used by the User to connect to the Server
	 */
	public int clientPort = 0;

	/**
	 * Constructor
	 * 
	 * @param nickname   The nickname of the User
	 * @param clientPort The Port used by the User to connect
	 * @throws IllegalArgumentException
	 */
	public User(String userNickname, int clientPort) throws IllegalArgumentException {
		super();

		if (userNickname == null || clientPort < 0 || clientPort > 65535)
			throw new IllegalArgumentException("Invalid Parameters");

		this.userNickname = userNickname;
		this.clientPort = clientPort;
	}

	/**
	 * Sets the input and output data streams of the Users
	 * 
	 * @param dataIn  The DataInputStream
	 * @param dataOut The DataOutputStream
	 */
	@Override
	public void setDataStreams(DataInputStream dataIn, DataOutputStream dataOut) {
		if (dataIn == null || dataOut == null)
			throw new IllegalArgumentException("Invalid Data Input or Output Stream.");

		this.dataIn = dataIn;
		this.dataOut = dataOut;
	}

	/**
	 * Terminates the Connection
	 */
	@Override
	public void terminateConnection() {
		try {
			this.dataIn.close();
			this.dataOut.close();
			
			if (!this.socket.isClosed()) {
				this.socket.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return The DataInputStream of the User
	 */
	@Override
	public DataInputStream getDataIn() {
		return this.dataIn;
	}

	/**
	 * @return The DataOutputStream of the User
	 */
	@Override
	public DataOutputStream getDataOut() {
		return this.dataOut;
	}

	/**
	 * @return The Public Key of the User
	 */
	@Override
	public PublicKey getPublicKey() {
		return this.publicKey;
	}

	/**
	 * Sets the Public Key of the User
	 * 
	 * @param publicKey The Public Key
	 */
	@Override
	public void setPublicKey(PublicKey publicKey) throws IllegalArgumentException {
		if (publicKey == null)
			throw new IllegalArgumentException("Invalid Parameter.");

		this.publicKey = publicKey;
	}

	/**
	 * @return The Private Key of the User
	 */
	@Override
	public PrivateKey getPrivateKey() {
		return this.privateKey;
	}

	/**
	 * Sets the Private Key of the User
	 * 
	 * @param privateKey The Private Key
	 */
	@Override
	public void setPrivateKey(PrivateKey privateKey) throws IllegalArgumentException {
		if (privateKey == null)
			throw new IllegalArgumentException("Invalid Parameter.");

		this.privateKey = privateKey;
	}

	/**
	 * @return The Public Key of the Server
	 */
	@Override
	public PublicKey getGuestPublicKey() {
		return this.guestPublicKey;
	}

	/**
	 * Sets the Public Key of the Server
	 * 
	 * @param guestPublicKey
	 */
	@Override
	public void setGuestPublicKey(PublicKey guestPublicKey) throws IllegalArgumentException {
		if (guestPublicKey == null)
			throw new IllegalArgumentException("Invalid Parameter.");

		this.guestPublicKey = guestPublicKey;
	}
	
	/**
	 * @return The socket of the User
	 */
	public Socket getSocket() {
		return this.socket;
	}

	/**
	 * Sets the socket of the User
	 * 
	 * @param socket The socket to be set
	 */
	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	/**
	 * @return The nickname of the User
	 */
	public String getNickname() {
		return this.userNickname;
	}

}
