package org.com.softwareengineeringproject;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;

import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.KeyPair;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class ServerChatSessionWindow {

	/**
	 * Form containing the Chat Session
	 */
	private JFrame frmChatSession = null;

	/**
	 * Button used to send messages
	 */
	private JButton sendButton = null;

	/**
	 * Button used to disconnect from the Chat Session
	 */
	private JButton disconnectButton = null;

	/**
	 * Text Area used to display messages
	 */
	private JTextArea messageTextArea = null;

	/**
	 * Text Box used to enter messages
	 */
	private JTextField messageTextBox = null;

	/**
	 * The User Object
	 */
	private User user = null;

	/**
	 * The Server Object
	 */
	private Server server = null;

	/**
	 * Socket used to control incoming/out-coming traffic
	 */
	private Socket socket = null;

	/**
	 * Creating the application.
	 */
	public ServerChatSessionWindow(int port, User user) {
		initialize();

		this.user = user;

		new Thread(new Runnable() {
			public void run() {
				try {
					// Launch the Server and Accepting Connections
					server = new Server(port);
					socket = server.getServerSocket().accept();
				
					// Generating A Public And A Private Key
					KeyPair keyPair = RSA.getInstance().generateKeyPair();
					server.setPublicKey(keyPair.getPublic());
					server.setPrivateKey(keyPair.getPrivate());

					// Enabling the User Interface
					messageTextBox.setEditable(true);
					sendButton.setEnabled(true);
					disconnectButton.setEnabled(true);

					// Setting Data Streams
					server.setDataStreams(new DataInputStream(socket.getInputStream()),
							new DataOutputStream(socket.getOutputStream()));

					// Sending Public Key To Guest User
					String myPublicKey = RSA.getInstance().publicKeyToString(server.getPublicKey());
					server.getDataOut().writeUTF(myPublicKey);

					// Acquiring Guest Public Key
					String guestPublicKeyString = server.getDataIn().readUTF();
					server.setGuestPublicKey(RSA.getInstance().stringToPublicKey(guestPublicKeyString));

					messageTextArea.setText("Client: Public Key Acquired From Guest");

					String incomingMessage = "";
		
					// Incoming Message Traffic
					while (true) {
						int byteLength = server.getDataIn().readInt();
						byte[] messageBytes = new byte[byteLength];
						server.getDataIn().readFully(messageBytes);

						incomingMessage = new String(RSA.getInstance().decrypt(server.getGuestPublicKey(), messageBytes));

						// Clearing the Text Area to accept new messages
						if (messageTextArea.getLineCount() < 15) {
							messageTextArea.setText(messageTextArea.getText() + "\n" + incomingMessage);
						} else {
							messageTextArea.setText("");
							messageTextArea.setText(incomingMessage);
						}
					}

				} catch (IOException e) {
					// Closing the Chat Session Window
					frmChatSession.dispose();
				
					// Returning back to the Main Menu
					new MainMenuWindow();

					// Displaying Information Dialog
					JOptionPane.showMessageDialog(null, "Chat Session was terminated.", "Information",
							JOptionPane.PLAIN_MESSAGE);

					try {
						socket.close();
						server.terminateConnection();

					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		}).start();

	}

	/**
	 * Initializing the contents of the frame.
	 */
	private void initialize() {
		// Form Initialization
		frmChatSession = new JFrame();
		frmChatSession.setTitle("Chat Session (Host)");
		frmChatSession.setResizable(false);
		frmChatSession.setBounds(100, 100, 510, 318);
		frmChatSession.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmChatSession.setVisible(true);

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 405, 94, 0 };
		gridBagLayout.rowHeights = new int[] { 20, 225, 29, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		frmChatSession.getContentPane().setLayout(gridBagLayout);

		disconnectButton = new JButton("Disconnect");
		disconnectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				server.terminateConnection();
			}
		});

		disconnectButton.setEnabled(false);
		GridBagConstraints gbc_disconnectButton = new GridBagConstraints();
		gbc_disconnectButton.anchor = GridBagConstraints.WEST;
		gbc_disconnectButton.fill = GridBagConstraints.VERTICAL;
		gbc_disconnectButton.insets = new Insets(0, 0, 5, 5);
		gbc_disconnectButton.gridx = 0;
		gbc_disconnectButton.gridy = 0;
		frmChatSession.getContentPane().add(disconnectButton, gbc_disconnectButton);

		messageTextArea = new JTextArea();
		messageTextArea.setEditable(false);
		messageTextArea.setFont(new Font("Arial", Font.PLAIN, 12));
		GridBagConstraints gbc_messageTextArea = new GridBagConstraints();
		gbc_messageTextArea.fill = GridBagConstraints.BOTH;
		gbc_messageTextArea.insets = new Insets(0, 0, 5, 0);
		gbc_messageTextArea.gridwidth = 2;
		gbc_messageTextArea.gridx = 0;
		gbc_messageTextArea.gridy = 1;
		frmChatSession.getContentPane().add(messageTextArea, gbc_messageTextArea);

		messageTextBox = new JTextField();
		messageTextBox.setEditable(false);
		GridBagConstraints gbc_messageTextBox = new GridBagConstraints();
		gbc_messageTextBox.fill = GridBagConstraints.BOTH;
		gbc_messageTextBox.insets = new Insets(0, 0, 0, 5);
		gbc_messageTextBox.gridx = 0;
		gbc_messageTextBox.gridy = 2;
		frmChatSession.getContentPane().add(messageTextBox, gbc_messageTextBox);
		messageTextBox.setColumns(10);

		messageTextBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String preEncryptedMessage = user.getNickname() + ": " + messageTextBox.getText();

					// Encrypting the message into a byte array
					byte[] encryptedMessage = RSA.getInstance().encrypt(server.getPrivateKey(), preEncryptedMessage);

					if (messageTextArea.getLineCount() < 15) {
						messageTextArea.setText(messageTextArea.getText() + "\n" + user.getNickname() + ": "
								+ messageTextBox.getText());
					} else {
						// Clearing the textArea if lines exceed 16
						messageTextArea.setText("");
						messageTextArea.setText(user.getNickname() + ": " + messageTextBox.getText());
					}

					// Sending the message byte length
					server.getDataOut().writeInt(encryptedMessage.length);

					// Sending the encrypted message in bytes
					server.getDataOut().write(encryptedMessage);

					messageTextBox.setText("");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		sendButton = new JButton("Send");
		sendButton.setEnabled(false);

		sendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String preEncryptedMessage = user.getNickname() + ": " + messageTextBox.getText();

					// Encrypting the message into a byte array
					byte[] encryptedMessage = RSA.getInstance().encrypt(server.getPrivateKey(), preEncryptedMessage);

					if (messageTextArea.getLineCount() < 15) {
						messageTextArea.setText(messageTextArea.getText() + "\n" + user.getNickname() + ": "
								+ messageTextBox.getText());
					} else {
						// Clearing the textArea if lines exceed 15
						messageTextArea.setText("");
						messageTextArea.setText(user.getNickname() + ": " + messageTextBox.getText());
					}

					// Sending the message byte length
					server.getDataOut().writeInt(encryptedMessage.length);

					// Sending the encrypted message in bytes
					server.getDataOut().write(encryptedMessage);

					messageTextBox.setText("");

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		GridBagConstraints gbc_sendButton = new GridBagConstraints();
		gbc_sendButton.fill = GridBagConstraints.BOTH;
		gbc_sendButton.gridx = 1;
		gbc_sendButton.gridy = 2;
		frmChatSession.getContentPane().add(sendButton, gbc_sendButton);
	}
}
