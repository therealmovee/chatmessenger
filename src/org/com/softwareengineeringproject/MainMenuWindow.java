/**
 * MainMenuWindow.java
 */
package org.com.softwareengineeringproject;

/**
 * @author ac01271
 */

import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.SystemColor;
import javax.swing.UIManager;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.EventQueue;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainMenuWindow {

	/**
	 * Frame used to store all the elements of the MainMenuWindow
	 */
	private JFrame frame = null;
	
	/**
	 * TextField used to enter the user's nickname
	 */
	private JTextField nicknameTextField = null;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainMenuWindow window = new MainMenuWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the application.
	 */
	public MainMenuWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		// Frame Initialization
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.textInactiveText);
		frame.getContentPane().setForeground(UIManager.getColor("TextPane.selectionBackground"));
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);

		// Panel Initialization
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 536, 194);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		// Title Label
		JLabel lblPrivateMessenger = new JLabel("Private Messenger");
		lblPrivateMessenger.setBounds(76, 16, 376, 54);
		lblPrivateMessenger.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 37));
		panel.add(lblPrivateMessenger);

		// TextField used to enter the user's nickname
		nicknameTextField = new JTextField();
		nicknameTextField.setFont(new Font("Tahoma", Font.PLAIN, 21));
		nicknameTextField.setBounds(58, 128, 417, 39);

		panel.add(nicknameTextField);
		nicknameTextField.setColumns(10);

		JLabel lblAccessKey = new JLabel("Nickname");
		lblAccessKey.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 14));
		lblAccessKey.setBounds(221, 86, 85, 39);
		panel.add(lblAccessKey);

		JButton btnNewButton = new JButton("Create Chatroom");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 31));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (nicknameTextField.getText().isEmpty()) {
					JOptionPane.showMessageDialog(frame, "Nickname not entered.", "Error", JOptionPane.ERROR_MESSAGE);
				} else {
					frame.dispose();
					new AccessKeyGenerationWindow(nicknameTextField.getText());
				}
			}
		});
		
		btnNewButton.setBounds(50, 221, 434, 71);
		frame.getContentPane().add(btnNewButton);

		JButton btnJoinChatroom = new JButton("Join Chatroom");
		btnJoinChatroom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (nicknameTextField.getText().isEmpty()) {
					JOptionPane.showMessageDialog(frame, "Nickname not entered.", "Error", JOptionPane.ERROR_MESSAGE);
				} else {
					frame.dispose();
					new JoinChatSessionWindow(nicknameTextField.getText());
				}

			}
		});

		btnJoinChatroom.setFont(new Font("Tahoma", Font.PLAIN, 31));
		btnJoinChatroom.setBounds(50, 334, 434, 71);
		frame.getContentPane().add(btnJoinChatroom);

		JLabel lblJoinChat = new JLabel("Join Chat");
		lblJoinChat.setBounds(117, 16, 186, 43);
		lblJoinChat.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 37));
		frame.setResizable(false);
		frame.setBackground(SystemColor.menu);
		frame.setBounds(100, 100, 542, 507);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
