package org.com.softwareengineeringproject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.security.PrivateKey;
import java.security.PublicKey;

public interface SocketClass {

	/**
	 * Sets the input and output data streams of the Server
	 * 
	 * @param dataIn  The DataInputStream
	 * @param dataOut The DataOutputStream
	 */
	public void setDataStreams(DataInputStream dataIn, DataOutputStream dataOut);

	/**
	 * Terminates the Server
	 */
	public void terminateConnection();

	/**
	 * @return The DataInputStream of the Server
	 */
	public DataInputStream getDataIn();

	/**
	 * @return The DataOutputStream of the Server
	 */
	public DataOutputStream getDataOut();

	/**
	 * @return The Public Key of the Server
	 */
	public PublicKey getPublicKey();

	/**
	 * Sets the Public Key of the Server
	 * 
	 * @param publicKey The Public Key
	 */
	public void setPublicKey(PublicKey publicKey) throws IllegalArgumentException;

	/**
	 * @return The Private Key of the User
	 */
	public PrivateKey getPrivateKey();

	/**
	 * Sets the Private Key of the User
	 * 
	 * @param privateKey The Private Key
	 */
	public void setPrivateKey(PrivateKey privateKey) throws IllegalArgumentException;

	/**
	 * @return The Public Key of the User
	 */
	public PublicKey getGuestPublicKey();

	/**
	 * Sets the Public Key of the Server
	 * 
	 * @param guestPublicKey
	 */
	public  void setGuestPublicKey(PublicKey guestPublicKey) throws IllegalArgumentException ;

}