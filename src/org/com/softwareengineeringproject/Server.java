/**
 * Server.java
 */

package org.com.softwareengineeringproject;

/**
 * @author ac01271
 */

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.security.PrivateKey;
import java.security.PublicKey;

public class Server implements SocketClass {

	/**
	 * Variable used to store the Data Input Stream of the Server
	 */
	private DataInputStream dataIn = null;

	/**
	 * Variable used to store the Data Output Stream of the Server
	 */
	private DataOutputStream dataOut = null;

	/**
	 * Variable used to store the Public Key of the Server
	 */
	private PublicKey publicKey = null;

	/**
	 * Variable used to store the Private Key of the Server
	 */
	private PrivateKey privateKey = null;

	/**
	 * Variable used to store the Public Key of the User
	 */
	private PublicKey guestPublicKey = null;

	/**
	 * ServerSocket used to control the incoming and out-coming traffic of the
	 * Server
	 */
	private ServerSocket serverSocket = null;

	/**
	 * Variable used to store the port used by the ServerSocket the host the session
	 */
	public int serverPort = 0;

	/**
	 * Constructor
	 * 
	 * @param serverPort The port that will be used to host the Server
	 * @throws IllegalArgumentException
	 */
	public Server(int serverPort) throws IllegalArgumentException {
		super();

		if (serverPort < 0 || serverPort > 65535)
			throw new IllegalArgumentException("Invalid Port");

		this.serverPort = serverPort;

		try {
			this.serverSocket = new ServerSocket(serverPort, 1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see org.com.softwareengineeringproject.SocketClass#setDataStreams(java.io.DataInputStream, java.io.DataOutputStream)
	 */
	
	public void setDataStreams(DataInputStream dataIn, DataOutputStream dataOut) {
		if (dataIn == null || dataOut == null)
			throw new IllegalArgumentException("Invalid Parameters.");

		this.dataIn = dataIn;
		this.dataOut = dataOut;
	}

	/* (non-Javadoc)
	 * @see org.com.softwareengineeringproject.SocketClass#terminateConnection()
	 */
	
	public void terminateConnection() {
		try {
			this.dataIn.close();
			this.dataOut.close();

			// Checking if the serverSocket is open.
			if (!serverSocket.isClosed()) {
				this.serverSocket.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/* (non-Javadoc)
	 * @see org.com.softwareengineeringproject.SocketClass#getDataIn()
	 */
	
	public DataInputStream getDataIn() {
		return this.dataIn;
	}

	/* (non-Javadoc)
	 * @see org.com.softwareengineeringproject.SocketClass#getDataOut()
	 */
	
	public DataOutputStream getDataOut() {
		return this.dataOut;
	}

	/* (non-Javadoc)
	 * @see org.com.softwareengineeringproject.SocketClass#getPublicKey()
	 */
	
	public PublicKey getPublicKey() {
		return this.publicKey;
	}

	/* (non-Javadoc)
	 * @see org.com.softwareengineeringproject.SocketClass#setPublicKey(java.security.PublicKey)
	 */
	
	public void setPublicKey(PublicKey publicKey) throws IllegalArgumentException {
		if (publicKey == null)
			throw new IllegalArgumentException("Invalid Parameter.");

		this.publicKey = publicKey;
	}

	/* (non-Javadoc)
	 * @see org.com.softwareengineeringproject.SocketClass#getPrivateKey()
	 */
	
	public PrivateKey getPrivateKey() {
		return this.privateKey;
	}

	/* (non-Javadoc)
	 * @see org.com.softwareengineeringproject.SocketClass#setPrivateKey(java.security.PrivateKey)
	 */
	
	public void setPrivateKey(PrivateKey privateKey) throws IllegalArgumentException {
		if (privateKey == null)
			throw new IllegalArgumentException("Invalid Parameter.");

		this.privateKey = privateKey;
	}

	/* (non-Javadoc)
	 * @see org.com.softwareengineeringproject.SocketClass#getGuestPublicKey()
	 */
	
	public PublicKey getGuestPublicKey() {
		return this.guestPublicKey;
	}

	/* (non-Javadoc)
	 * @see org.com.softwareengineeringproject.SocketClass#setGuestPublicKey(java.security.PublicKey)
	 */
	
	public void setGuestPublicKey(PublicKey guestPublicKey) {
		if (guestPublicKey == null)
			throw new IllegalArgumentException("Invalid Public Key");

		this.guestPublicKey = guestPublicKey;
	}
	
	/**
	 * @return The ServerSocket of the Server
	 */
	public ServerSocket getServerSocket() {
		return this.serverSocket;
	}
}
