package org.com.softwareengineeringproject;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.KeyPair;
import java.security.PublicKey;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class UserChatSessionWindow {

	/**
	 * Frame that contains the Chat Session Window
	 */
	private JFrame frmChatSession = null;

	/**
	 * Text Area used to display messages
	 */
	private JTextArea messageTextArea = null;

	/**
	 * Text Box used to input messages
	 */
	private JTextField messageTextbox = null;
	
	/**
	 * Button used to disconnect from the Chat Session
	 */
	private JButton disconnectButton = null;

	/**
	 * The User Object
	 */
	private User user = null;

	/**
	 * Create the application.
	 */
	public UserChatSessionWindow(int port, User user) {
		initialize();
		
		this.user = user;

		// Starting the server and connecting the user.
		new Thread(new Runnable() {
			public void run() {
				try {
					// Generating A Public And A Private Key
					KeyPair keyPair = RSA.getInstance().generateKeyPair();
					user.setPublicKey(keyPair.getPublic());
					user.setPrivateKey(keyPair.getPrivate());
					
					// Setting up the client socket
					user.setSocket(new Socket("127.0.0.1", port));

					// Setting up the data streams
					user.setDataStreams(new DataInputStream(user.getSocket().getInputStream()),
							new DataOutputStream(user.getSocket().getOutputStream()));

					// Acquiring Guest Public Key
					String guestPublicKeyString = user.getDataIn().readUTF();
					PublicKey guestPublicKey = RSA.getInstance().stringToPublicKey(guestPublicKeyString);

					messageTextArea.setText("Client: Public Key Acquired From Guest");

					// Sending Public Key To Guest
					String myPublicKey = RSA.getInstance().publicKeyToString(user.getPublicKey());
					user.getDataOut().writeUTF(myPublicKey);

					String incomingMessage = "";
					
					while (true) {
						int byteLength = user.getDataIn().readInt();
						byte[] messageBytes = new byte[byteLength];
						user.getDataIn().readFully(messageBytes);

						incomingMessage = new String(RSA.getInstance().decrypt(guestPublicKey, messageBytes));

						if (messageTextArea.getLineCount() < 15) {
							messageTextArea.setText(messageTextArea.getText() + "\n" + incomingMessage);
						} else {
							// Clearing the textArea if lines exceed 15
							messageTextArea.setText("");
							messageTextArea.setText(incomingMessage);
						}
		
					}
					
				} catch (IOException e) {
					// Closing the Chat Session Window
					frmChatSession.dispose();
					
					// Returning back to the Main Menu
					new MainMenuWindow();
					
					// Displaying Information Dialog
					JOptionPane.showMessageDialog(null, "Chat Session was terminated", "Information", JOptionPane.PLAIN_MESSAGE);
					
					// Closing the Server Sockets
					user.terminateConnection();
				}

			}
		}).start();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmChatSession = new JFrame();
		frmChatSession.setTitle("Chat Session (Client)");
		frmChatSession.setBounds(100, 100, 523, 336);
		frmChatSession.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmChatSession.setVisible(true);
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 410, 94, 0 };
		gridBagLayout.rowHeights = new int[] { 28, 234, 28, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		frmChatSession.getContentPane().setLayout(gridBagLayout);

		disconnectButton = new JButton("Disconnect");
		disconnectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				user.terminateConnection();
			}
		});

		JLabel lblInformation = new JLabel("");
		GridBagConstraints gbc_lblInformation = new GridBagConstraints();
		gbc_lblInformation.anchor = GridBagConstraints.NORTH;
		gbc_lblInformation.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblInformation.insets = new Insets(0, 0, 5, 0);
		gbc_lblInformation.gridwidth = 2;
		gbc_lblInformation.gridx = 0;
		gbc_lblInformation.gridy = 0;
		frmChatSession.getContentPane().add(lblInformation, gbc_lblInformation);
		GridBagConstraints gbc_disconnectButton = new GridBagConstraints();
		gbc_disconnectButton.anchor = GridBagConstraints.WEST;
		gbc_disconnectButton.fill = GridBagConstraints.VERTICAL;
		gbc_disconnectButton.insets = new Insets(0, 0, 5, 5);
		gbc_disconnectButton.gridx = 0;
		gbc_disconnectButton.gridy = 0;
		frmChatSession.getContentPane().add(disconnectButton, gbc_disconnectButton);

		messageTextArea = new JTextArea();
		messageTextArea.setFont(new Font("Arial", Font.PLAIN, 12));
		messageTextArea.setEditable(false);
		GridBagConstraints gbc_messageTextArea = new GridBagConstraints();
		gbc_messageTextArea.fill = GridBagConstraints.BOTH;
		gbc_messageTextArea.insets = new Insets(0, 0, 5, 0);
		gbc_messageTextArea.gridwidth = 2;
		gbc_messageTextArea.gridx = 0;
		gbc_messageTextArea.gridy = 1;
		frmChatSession.getContentPane().add(messageTextArea, gbc_messageTextArea);
		messageTextArea.setColumns(20);

		messageTextbox = new JTextField();
		GridBagConstraints gbc_messageTextbox = new GridBagConstraints();
		gbc_messageTextbox.fill = GridBagConstraints.BOTH;
		gbc_messageTextbox.insets = new Insets(0, 0, 0, 5);
		gbc_messageTextbox.gridx = 0;
		gbc_messageTextbox.gridy = 2;
		frmChatSession.getContentPane().add(messageTextbox, gbc_messageTextbox);
		messageTextbox.setColumns(10);

		messageTextbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String preEncryptedMessage = user.getNickname() + ": " + messageTextbox.getText();

					// Encrypting the message into a byte array
					byte[] encryptedMessage = RSA.getInstance().encrypt(user.getPrivateKey(), preEncryptedMessage);

					if (messageTextArea.getLineCount() < 15) {
						messageTextArea.setText(messageTextArea.getText() + "\n" + user.getNickname() + ": "
								+ messageTextbox.getText());
					} else {
						// Clearing the textArea if lines exceed 16
						messageTextArea.setText("");
						messageTextArea.setText(user.getNickname() + ": " + messageTextbox.getText());
					}
					// Sending the message byte length
					user.getDataOut().writeInt(encryptedMessage.length);

					// Sending the encrypted message in bytes
					user.getDataOut().write(encryptedMessage);

					messageTextbox.setText("");
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		});

		JButton sendButton = new JButton("Send");

		sendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String preEncryptedMessage = user.getNickname() + ": " + messageTextbox.getText();

					// Encrypting the message into a byte array
					byte[] encryptedMessage = RSA.getInstance().encrypt(user.getPrivateKey(), preEncryptedMessage);

					if (messageTextArea.getLineCount() < 15) {
						messageTextArea.setText(messageTextArea.getText() + "\n" + user.getNickname() + ": "
								+ messageTextbox.getText());
					} else {
						// Clearing the textArea if lines exceed 16
						messageTextArea.setText("");
						messageTextArea.setText(user.getNickname() + ": " + messageTextbox.getText());
					}

					// Sending the message byte length
					user.getDataOut().writeInt(encryptedMessage.length);

					// Sending the encrypted message in bytes
					user.getDataOut().write(encryptedMessage);

					messageTextbox.setText("");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		GridBagConstraints gbc_sendButton = new GridBagConstraints();
		gbc_sendButton.fill = GridBagConstraints.BOTH;
		gbc_sendButton.gridx = 1;
		gbc_sendButton.gridy = 2;
		frmChatSession.getContentPane().add(sendButton, gbc_sendButton);

	}

}
