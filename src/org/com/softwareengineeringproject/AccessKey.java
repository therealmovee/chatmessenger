/**
 * AccessKey.java
 */
package org.com.softwareengineeringproject;

/**
 * @author ac01271
 */

public class AccessKey {

	/**
	 * Single Instance
	 */
	private static final AccessKey instance = new AccessKey();
	
	/**
	 * Private Constructor
	 */
	private AccessKey() {}
	
	/**
	 * @param port The Port of the Chat Session
	 * @return The Access Key
	 */
	public String encodeAccessKey(int port) throws IllegalArgumentException {
		if (port < 0)
			throw new IllegalArgumentException("Invalid Parameter");
		
		return ("AA" + (port * 256 * 256));
	}

	/**
	 * @param accessKey The Access Key
	 * @return The Port of the Chat Session
	 */
	public int decodeAccessKey(String accessKey) throws IllegalArgumentException {		
		if(accessKey == null)
			throw new IllegalArgumentException("Invalid Paramater.");
		
		return (Integer.parseInt(accessKey.replaceAll("AA", "")) / 256 / 256);
	}
	
	public static AccessKey getInstance() {
		return AccessKey.instance;
	}
}
