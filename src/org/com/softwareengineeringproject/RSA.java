/**
 * RSA.java
 */

package org.com.softwareengineeringproject;

/**
 * @author ac01271
 */

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class RSA {


	/**
	 * Single Instance
	 */
	private static final RSA instance = new RSA();
	
	/**
	 * Private Constructor
	 */
	private RSA() {}
	
	/**
	 * @return The KeyPair containing both the PublicKey and the PrivateKey
	 */
	public KeyPair generateKeyPair() {
		final int keySize = 2048;

		try {
			// Using the RSA Algorithm
			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");

			// Initializing the size of the keyPairGenerator
			keyPairGenerator.initialize(keySize);

			return keyPairGenerator.genKeyPair();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * @param privateKey The Private Key which will be used to encrypt the message
	 * @param message    The message that will be encrypted
	 * @return The encrypted message in byte array form
	 */
	public byte[] encrypt(PrivateKey privateKey, String message) throws IllegalArgumentException {
		if (privateKey == null || message == null)
			throw new IllegalArgumentException("Invalid Private Key");
		
		Cipher cipher = null;

		try {
			// Using the RSA Algorithm
			cipher = Cipher.getInstance("RSA");

			// Using the Private Key to encrypt the message
			cipher.init(Cipher.ENCRYPT_MODE, privateKey);

			return cipher.doFinal(message.getBytes());
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();	
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * @param publicKey The Public Key which will be used to decode the message
	 * @param encryptedMessage    The message that will be decoded
	 * @return The decoded message in byte array form
	 */
	public byte[] decrypt(PublicKey publicKey, byte[] encryptedMessage) throws IllegalArgumentException {
		if (publicKey == null || encryptedMessage == null)
			throw new IllegalArgumentException("Invalid Public Key");
		
		Cipher cipher = null;

		try {
			// Using the RSA Algorithm
			cipher = Cipher.getInstance("RSA");
			
			// Using the Public Key to decode the message
			cipher.init(Cipher.DECRYPT_MODE, publicKey);
			
			return cipher.doFinal(encryptedMessage);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * @param publicKey	The Public Key
	 * @return	The Public Key In A String Form
	 */
	public String publicKeyToString(PublicKey publicKey) throws IllegalArgumentException {
		if (publicKey == null )
			throw new IllegalArgumentException("Invalid Public Key");
		
		KeyFactory keyFactory = null;

		try {
			// Using RSA Algorithm to convert the public key into a string
			keyFactory = KeyFactory.getInstance("RSA");
			X509EncodedKeySpec keySpec = keyFactory.getKeySpec(publicKey, X509EncodedKeySpec.class);
			
			// Encoding the keySpec into a String
			return Base64.getEncoder().encodeToString(keySpec.getEncoded());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * @param publicKeyString	The Public Key In A String Form
	 * @return	The Public Key
	 */
	public PublicKey stringToPublicKey(String publicKeyString) {
		if (publicKeyString == null)
			throw new IllegalArgumentException("Invalid Parameter");
		
		try {
			// Decoding the Public Key String into a byte array using Base 64 decoder
			byte[] publicKeyBytes = Base64.getDecoder().decode(publicKeyString);
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
			
			// Using the RSA Algorithm to generate a public key
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");

			return keyFactory.generatePublic(keySpec);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public static RSA getInstance() {
		return RSA.instance;
	}
}
