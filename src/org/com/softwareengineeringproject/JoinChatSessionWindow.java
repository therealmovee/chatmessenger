/**
 * JoinChatSessionWindow.java
 */
package org.com.softwareengineeringproject;

/**
 * @author ac01271
 */

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class JoinChatSessionWindow {

	/**
	 * Frame used to store all the elements of the JoinChatSessionWindow
	 */
	private JFrame frame = null;

	/**
	 * Text field used to enter the Access Key
	 */
	private JTextField textField = null;

	/**
	 * Variable used to transfer the nickname that was entered from the main menu.
	 */
	private static String temporaryNickname = "";

	/**
	 * Create the application.
	 */
	public JoinChatSessionWindow(String temporaryNickname) {
		initialize();
		
		JoinChatSessionWindow.temporaryNickname = temporaryNickname;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		// Frame Initialization
		frame = new JFrame();
		frame.setTitle("Enter The Access Key");
		frame.setBounds(100, 100, 417, 116);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setVisible(true);

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 176, 122, 111, 0 };
		gridBagLayout.rowHeights = new int[] { 86, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		frame.getContentPane().setLayout(gridBagLayout);

		// Return Button Action Listener
		JButton btnReturnBack = new JButton("Return Back");
		btnReturnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Returning to Main Menu
				frame.dispose();
				new MainMenuWindow();
			}
		});

		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		textField.setEditable(true);
		textField.setColumns(10);

		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.BOTH;
		gbc_textField.insets = new Insets(0, 0, 0, 5);
		gbc_textField.gridx = 0;
		gbc_textField.gridy = 0;
		frame.getContentPane().add(textField, gbc_textField);

		// Generate Button Action Listener
		JButton btnGenerateAccessKey = new JButton("Join Chatroom");
		btnGenerateAccessKey.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int port = AccessKey.getInstance().decodeAccessKey(textField.getText());
				
				// Checking if the Access Key has a valid length
				if (textField.getText().length() < 6) {
					JOptionPane.showMessageDialog(null, "AccessKey Invalid.", "Error", JOptionPane.ERROR_MESSAGE);
				} else {
					// Decoding the Access Key into a Port number
					User user = new User(getTemporaryNickname(), port);

					// Navigating to the UserChatSession Window
					frame.dispose();
					new UserChatSessionWindow(port, user);
				}

			}
		});

		GridBagConstraints gbc_btnGenerateAccessKey = new GridBagConstraints();
		gbc_btnGenerateAccessKey.fill = GridBagConstraints.BOTH;
		gbc_btnGenerateAccessKey.insets = new Insets(0, 0, 0, 5);
		gbc_btnGenerateAccessKey.gridx = 1;
		gbc_btnGenerateAccessKey.gridy = 0;
		frame.getContentPane().add(btnGenerateAccessKey, gbc_btnGenerateAccessKey);
		GridBagConstraints gbc_btnReturnBack = new GridBagConstraints();
		gbc_btnReturnBack.fill = GridBagConstraints.BOTH;
		gbc_btnReturnBack.gridx = 2;
		gbc_btnReturnBack.gridy = 0;
		frame.getContentPane().add(btnReturnBack, gbc_btnReturnBack);
	}

	/**
	 * @return The temporary nickname
	 */
	public String getTemporaryNickname() {
		return JoinChatSessionWindow.temporaryNickname;
	}
}
