package org.com.softwareengineeringproject;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class AccessKeyGenerationWindow {

	/**
	 * The Frame containing the Access Key Generation Window
	 */
	private JFrame frmAccessKeyGeneration = null;

	/**
	 * Text Field used to enter the Access Key
	 */
	private JTextField textField = null;

	/**
	 * Temporary String Variable used to transfer the user's nickname
	 */
	private static String temporaryNickname = "";

	/**
	 * Create the application.
	 */
	public AccessKeyGenerationWindow(String temporaryNickname) {
		initialize();
		
		AccessKeyGenerationWindow.temporaryNickname = temporaryNickname;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		// Frame Initialization
		frmAccessKeyGeneration = new JFrame();
		frmAccessKeyGeneration.setTitle("Access Key Generation (Create Chatroom)");
		frmAccessKeyGeneration.setBounds(100, 100, 442, 145);
		frmAccessKeyGeneration.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAccessKeyGeneration.setVisible(true);
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 420, 0 };
		gridBagLayout.rowHeights = new int[] { 34, 42, 23, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		frmAccessKeyGeneration.getContentPane().setLayout(gridBagLayout);

		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		textField.setEditable(false);
		
		// Generating a Random Port
		int port = (int) ((Math.random() * 2000) * 3);

		textField.setText(AccessKey.getInstance().encodeAccessKey(port));
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.BOTH;
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.gridx = 0;
		gbc_textField.gridy = 0;
		frmAccessKeyGeneration.getContentPane().add(textField, gbc_textField);
		textField.setColumns(10);

		JButton btnGenerateAccessKey = new JButton("Start Chatroom");
		btnGenerateAccessKey.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Closing the frame
				frmAccessKeyGeneration.dispose();
				
				// Opening the Server Chat Session Window
				int port = AccessKey.getInstance().decodeAccessKey(textField.getText());

				User user = new User(temporaryNickname, port);
				
				new ServerChatSessionWindow(port, user);
			}
		});

		btnGenerateAccessKey.setSize(53, 1234);
		GridBagConstraints gbc_btnGenerateAccessKey = new GridBagConstraints();
		gbc_btnGenerateAccessKey.fill = GridBagConstraints.BOTH;
		gbc_btnGenerateAccessKey.insets = new Insets(0, 0, 5, 0);
		gbc_btnGenerateAccessKey.gridx = 0;
		gbc_btnGenerateAccessKey.gridy = 1;
		frmAccessKeyGeneration.getContentPane().add(btnGenerateAccessKey, gbc_btnGenerateAccessKey);

		JButton btnReturnBack = new JButton("Return Back");
		btnReturnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmAccessKeyGeneration.dispose();
				new MainMenuWindow();
			}
		});
		GridBagConstraints gbc_btnReturnBack = new GridBagConstraints();
		gbc_btnReturnBack.fill = GridBagConstraints.BOTH;
		gbc_btnReturnBack.gridx = 0;
		gbc_btnReturnBack.gridy = 2;
		frmAccessKeyGeneration.getContentPane().add(btnReturnBack, gbc_btnReturnBack);
	}
}
